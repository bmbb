#!/bin/sh

set -e
qemu-system-i386 -s -serial file:serial.txt -m 16M -hda ../BlackBox.img -hdb ../minix.img
