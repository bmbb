#!/bin/sh

set -e
objdump -b binary -m i8086 -D ../BlackBox.img | less
