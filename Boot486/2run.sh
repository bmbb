#!/bin/sh

#bochs -q 'boot:a' 'floppya: 1_44=../BlackBox.img, status=inserted'
#bochs -q 'boot:c' 'ata0-master: type=disk, path="boot.bin", mode=flat' 'display_library: x, options="gui_debug"' 2> bochs.log

#bochs -q \
#	'boot:a' \
#	'floppya: 1_44=boot.bin, status=inserted' \
#	'display_library: x, options="gui_debug"' \
#	'com1: enabled=1, mode=file, dev=serial.txt' \
#	2> bochs.log

bochs -q \
	'boot:c' \
	'ata0-master: type=disk, path="../BlackBox.img", mode=flat' \
	'com1: enabled=1, mode=file, dev=serial.txt' \
	'display_library: x, options="gui_debug"' \
	'magic_break: enabled=1' \
	2> bochs.log
