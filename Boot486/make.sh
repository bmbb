#!/bin/sh

set -e
as -o boot0.o boot0.S
ld --oformat binary -o ../BlackBox.img -Ttext 0x7c00 boot0.o
rm -f boot0.o
