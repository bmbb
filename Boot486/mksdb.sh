#!/bin/sh

DRIVE=/dev/sdb

if ! [ -e $DRIVE ]; then
	echo "${DRIVE} not available"
	exit 1
fi

echo "overwrite ${DRIVE}"
sudo dd if=boot.bin of=$DRIVE
