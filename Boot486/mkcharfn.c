#include <locale.h>
#include <wchar.h>
#include <wctype.h>
#include <stdio.h>

#define MAX_CP_CAHR 0x10000

static wint_t isalphatab[MAX_CP_CAHR];
static wint_t isuppertab[MAX_CP_CAHR];
static wint_t islowertab[MAX_CP_CAHR];
static wint_t uppertable[MAX_CP_CAHR];
static wint_t lowertable[MAX_CP_CAHR];

static void
print_tabs(int n) {
	for (int i = 0; i < n; i++) {
		putchar('\t');
	}
}

static void
print_range (wint_t *tab, int max, int t, int retbool) {
	int i = 0;
	int begin = 0;
	int end = 0;
	int oddmode = 0;
	int fold = 0;
	int count = 0;

	print_tabs(t);
	printf("CASE ch OF\n");

	while (i < max) {
		while ((i < max) && (tab[i] == 0)) ++i;
		begin = i;

		oddmode = 0;
		if (i + 2 < max && tab[i + 1] == 0 && tab[i + 2] != 0) {
			if (tab[i] % 2 == 1) {
				oddmode = 1;
			} else {
				oddmode = 2;
			}
		}

		if (oddmode == 0) {
			while (i < max && tab[i] != 0) i += 1;
			end = i - 1;
		} else {
			while (i + 1 < max && tab[i] != 0 && tab[i + 1] == 0) i += 2;
			end = i;
		}

		if (i < max && begin <= end) {
			print_tabs(t);
			// printf("(* %i *) ", oddmode)
			printf("| ");

			if (begin == end) {
				if (retbool) {
					printf("0%XX: RETURN TRUE\n", begin);
				} else {
					printf("0%XX: RETURN 0%XX\n", begin, tab[begin]);
				}
			} else {
				printf("0%XX..0%XX: ", begin, end);
				if (retbool) {
					if (oddmode == 0) {
						printf("RETURN TRUE\n");
					} else if (oddmode == 1) {
						printf("RETURN ODD(ORD(ch))\n");
					} else if (oddmode == 2) {
						printf("RETURN ~ODD(ORD(ch))\n");
					}
				} else {
					if (oddmode == 0) {
						printf("RETURN CHR(0%XH + ORD(ch) - 0%XH)\n", tab[begin], begin);
					} else if (oddmode == 1) {
						printf("IF ODD(ORD(ch)) THEN RETURN CHR(0%XH + ORD(ch) - 0%XH) ELSE RETURN ch END\n", tab[begin], begin);
					} else if (oddmode == 2) {
						printf("IF ~ODD(ORD(ch)) THEN RETURN CHR(0%XH + ORD(ch) - 0%XH) ELSE RETURN ch END\n", tab[begin], begin);
					}
				}
			}

			count += 1;
			if (count > 4096) {
				count = 0;
				fold += 1;
				print_tabs(t);
				printf("ELSE\n");
				t += 1;
				print_tabs(t);				
				printf("CASE ch OF\n");	
			}
		}

		i += 1;
	}

	for (int i = 0; i < fold + 1; i++) {
		print_tabs(t);
		printf("ELSE ");
		if (retbool) {
			printf("RETURN FALSE\n");
		} else {
			printf("RETURN ch\n");
		}
		print_tabs(t);
		printf("END\n");
		t -= 1;
	}
}

int
main (int argc, char **argv) {
	setlocale(LC_ALL, "");
	for (wint_t i = 0; i < MAX_CP_CAHR; i++) {
		isalphatab[i] = iswalpha(i) ? i : 0;
		isuppertab[i] = iswupper(i) ? i : 0;
		islowertab[i] = iswlower(i) ? i : 0;
		uppertable[i] = (towupper(i) == i) ? 0 : towupper(i);
		lowertable[i] = (towlower(i) == i) ? 0 : towlower(i);
	}

	printf("MODULE Unicode;\n\n");

	printf("\tIMPORT Kernel;\n\n");

	printf("\tTYPE\n");
	printf("\t\tHook = POINTER TO RECORD (Kernel.CharHook) END;\n\n");

	printf("\tPROCEDURE (h: Hook) Upper (ch: CHAR): CHAR;\n");
	printf("\tBEGIN\n");
	print_range(uppertable, MAX_CP_CAHR, 2, 0);
	printf("\tEND Upper;\n\n");

	printf("\tPROCEDURE (h: Hook) Lower (ch: CHAR): CHAR;\n");
	printf("\tBEGIN\n");
	print_range(lowertable, MAX_CP_CAHR, 2, 0);
	printf("\tEND Lower;\n\n");

	printf("\tPROCEDURE (h: Hook) IsAlpha (ch: CHAR): BOOLEAN;\n");
	printf("\tBEGIN\n");
	print_range(isalphatab, MAX_CP_CAHR, 2, 1);
	printf("\tEND IsAlpha;\n\n");

	printf("\tPROCEDURE (h: Hook) IsUpper (ch: CHAR): BOOLEAN;\n");
	printf("\tBEGIN\n");
	print_range(isuppertab, MAX_CP_CAHR, 2, 1);
	printf("\tEND IsUpper;\n\n");

	printf("\tPROCEDURE (h: Hook) IsLower (ch: CHAR): BOOLEAN;\n");
	printf("\tBEGIN\n");
	print_range(islowertab, MAX_CP_CAHR, 2, 1);
	printf("\tEND IsLower;\n\n");

	printf("\tPROCEDURE Init;\n");
	printf("\t\tVAR h: Hook;\n");
	printf("\tBEGIN\n");
	printf("\t\tNEW(h); Kernel.SetCharHook(h)\n");
	printf("\tEND Init;\n\n");

	printf("BEGIN\n");
	printf("\tInit\n");
	printf("CLOSE\n");
	printf("\tKernel.SetCharHook(NIL)\n");
	printf("END Unicode.");
}
